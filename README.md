# README #

Accompanying source code for blog entry at:

- https://tech.asimio.net/2019/04/23/Reporting-Code-Coverage-using-Maven-and-JaCoCo-plugin.html
- https://tech.asimio.net/2023/07/05/Uploading-JaCoCo-Code-Coverage-Reports-to-SonarQube.html

## https://tech.asimio.net/2019/04/23/Reporting-Code-Coverage-using-Maven-and-JaCoCo-plugin.html ##

### Requirements ###

* Java 7+
* Maven 3.2+

### JaCoCo - Building and running from command line ###

```
mvn clean verify
```

Open report at `target/site/jacoco/index.html`


## https://tech.asimio.net/2023/07/05/Uploading-JaCoCo-Code-Coverage-Reports-to-SonarQube.html ##

### Requirements ###

* Java 11+
* Maven 3.2+
* SonarQube server instance

### JaCoCo - Build artifact and generate reports ###

```
mvn -f pom-no-tests-splitting.xml clean package
```

### SonarQube - Upload JaCoCo code coverage reports ###

```
mvn sonar:sonar -Dsonar.projectKey=code-coverage-maven-jacoco -Dsonar.coverage.jacoco.xmlReportPaths=target/site/jacoco/jacoco.xml -Dsonar.host.url=http://localhost:9000 -Dsonar.login=admin -Dsonar.password=sonar
```

Open SonarQube project UI at http://localhost:9000/dashboard?id=code-coverage-maven-jacoco 


### Who do I talk to? ###

* orlando.otero at asimio dot net
* https://www.linkedin.com/in/ootero
